<?php

require_once 'vendor/autoload.php';

$app = new \Slim\Slim();

$db = new mysqli('localhost', 'root', 'S3milla', 'curso_angular');

// configuracion de cabeceras
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}


$app->get("/pruebas", function() use($app, $db){
    echo "hola mundo desde Slim php ";
    var_dump($db);
});

$app->get("/probando", function() use($app){
    echo "Otroooooo ";
});

// listar todos los productos
$app->get('/productos', function() use($app, $db){
    $sql = "SELECT * FROM productos ORDER BY id DESC";
    $query = $db->query($sql);
    $productos = [];
    while ($producto = $query->fetch_assoc()) {
        $productos[] = $producto;
    }

    $result = [
        'status' => 'success',
        'code' => 200,
        'data' => $productos
    ];
    echo json_encode($result);
});


// devolver un solo producto
$app->get('/producto/:id', function($id) use($app, $db){
    $sql = "SELECT * FROM productos WHERE id = '{$id}'";
    $query = $db->query($sql);
    $result = [
        'status' => 'error',
        'code' => 404,
        'message' => 'Producto no disponible'
    ];
    if ($query->num_rows == 1) {
        $producto = $query->fetch_assoc();
        $result = [
            'status' => 'success',
            'code' => 200,
            'data' => $producto
        ];
    }
    echo json_encode($result);
});


// eliminar un producto
$app->get('/delete-producto/:id', function($id) use($app, $db){
    $sql = "DELETE FROM productos WHERE id = '{$id}'";
    $query = $db->query($sql);

    if ($query) {
        $result = [
            'status' => 'success',
            'code' => 200,
            'message' => 'El producto se ha eliminado correctamente!!'
        ];
    }else{
        $result = [
            'status' => 'error',
            'code' => 404,
            'message' => 'Producto no se ha eliminado'
        ];
    }
    echo json_encode($result);
});


// actualizar un producto
$app->post('/update-producto/:id', function($id) use($app, $db){
    $json = $app->request->post('json');
    $data = json_decode($json, true);

    $sqlImagen = (isset($data['imagen'])) ? ", imagen = '{$data['imagen']}'" : '' ;

    $sql = "UPDATE productos SET 
                nombre = '{$data['nombre']}',
                descripcion = '{$data['descripcion']}',
                precio = '{$data['precio']}'
                $sqlImagen
            WHERE id = '{$id}'";
            
    $query = $db->query($sql);

    if ($query) {
        $result = [
            'status' => 'success',
            'code' => 200,
            'message' => 'El producto se ha actualizado correctamente!!'
        ];
    }else{
        $result = [
            'status' => 'error',
            'code' => 404,
            'message' => 'Producto no se ha actualizado'
        ];
    }
    echo json_encode($result);
});

// subir una imagen a un producto
$app->post('/upload-file', function() use($app, $db){

    $result = [
        'status' => 'error',
        'code' => 404,
        'message' => 'el archivo no ha podido subirse'
    ];

    if (isset($_FILES['uploads'])) {
        $piramideUploader = new PiramideUploader();
        $upload = $piramideUploader->upload('image', 'uploads', 'uploads', ['image/jpeg', 'image/png', 'image/gif']);   
        $file = $piramideUploader->getInfoFile();     
        $fileName = $file['complete_name'];

        if (isset($upload) && $upload['uploaded'] == false) {
            $result = [
                'status' => 'error',
                'code' => 404,
                'message' => 'el archivo no ha podido subirse'
            ];
        }else{
            $result = [
                'status' => 'success',
                'code' => 200,
                'message' => 'el archivo se ha sudido',
                'filename' => $fileName
            ];
        }
    }

    echo json_encode($result);
});



// guardar productos
$app->post('/productos', function() use($app, $db){
    $json = $app->request->post('json');
    $data = json_decode($json, true);

    if (!isset($data['imagen'])) {
        $data['imagen'] = null;
    }

    if (!isset($data['descripcion'])) {
        $data['descripcion'] = null;
    }

    if (!isset($data['nombre'])) {
        $data['nombre'] = null;
    }

    if (!isset($data['precio'])) {
        $data['precio'] = null;
    }

    $query = "INSERT INTO productos VALUES (null, 
        '{$data['nombre']}',
        '{$data['descripcion']}',
        '{$data['precio']}',
        '{$data['imagen']}');";

    $result = [
        'status' => 'error',
        'code' => 404,
        'message' => 'Producto no insertado'
    ];

    $insert = $db->query($query);

    if ($insert) {
        $result = [
            'status' => 'success',
            'code' => 200,
            'message' => 'Producto creado correctamente'
        ];
    }

    echo json_encode($result);
});

$app->run();