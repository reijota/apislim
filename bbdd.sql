CREATE DATABASE `curso_angular` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE 'productos' (
  'id' int NOT NULL AUTO_INCREMENT,
  'nombre' varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  'descripcion' text COLLATE utf8mb4_bin,
  'precio' varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  'imagen' varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
